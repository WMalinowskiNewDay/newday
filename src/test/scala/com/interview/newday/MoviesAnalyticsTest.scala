package com.interview.newday

import com.interview.newday.entities.MovieRating
import org.apache.spark.sql.{Row, SparkSession}
import org.scalatest.FunSuite


class MoviesAnalyticsTest extends FunSuite {
  val spark = SparkSession
    .builder()
    .master("local[1]")
    .appName(s"Spark Test")
    .config("spark.local.ip", "localhost")
    .config("spark.driver.host", "localhost")
    .getOrCreate()

  import spark.implicits._

  test("aggregateMovieRatings should display correct min, max and avg grouped by movieId") {
    val inputDs = Seq(MovieRating(0, 1, 1, 0L), MovieRating(0, 1, 5, 0L), MovieRating(0, 2, 4, 0L)).toDS()
    val aggMovieRatings = MoviesAnalytics.aggregateMovieRatings(inputDs).collect()

    assert(aggMovieRatings.contains(Row(1, 1, 5, 1, 3.0)))
    assert(aggMovieRatings.contains(Row(2, 2, 4, 4, 4.0)))
    assert(aggMovieRatings.length === 2)
  }

}
