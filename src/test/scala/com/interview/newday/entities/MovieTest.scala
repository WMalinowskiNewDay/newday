package com.interview.newday.entities

import org.scalatest.FunSuite

class MovieTest extends FunSuite {

  test("Movie object correctly parses line from movies.dat") {
    val line1Movie = Movie("1::Toy Story (1995)::Animation|Children's|Comedy")
    val tartget1Movie = Movie(1, s"Toy Story (1995)", Array("Animation", "Children's", "Comedy"))
    assert(line1Movie === tartget1Movie)

    val line2Movie = Movie("2::Jumanji (1995)::Adventure|Children's|Fantasy")
    val tartget2Movie = Movie(2, s"Jumanji (1995)", Array("Adventure", "Children's", "Fantasy"))
    assert(line2Movie === tartget2Movie)
  }

}
