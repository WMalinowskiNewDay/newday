package com.interview.newday.entities

import org.scalatest.FunSuite

class UserTest extends FunSuite {

  test("User object correctly parses line from users.dat") {
    val line1User = User("1::F::1::10::48067")
    val tartget1User = User(1, 'F', 1, 10, "48067")
    assert(line1User === Some(tartget1User))

    val line2User = User("2::M::56::16::70072")
    val tartget2User = User(2,'M', 56, 16, "70072")
    assert(line2User === Some(tartget2User))
  }

}
