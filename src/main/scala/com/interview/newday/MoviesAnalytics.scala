package com.interview.newday

import com.interview.newday.entities.{Movie, MovieRating}
import org.apache.spark.sql.{Dataset, SparkSession}
import org.apache.spark.sql.functions.{max, mean, min}
import org.joda.time.{DateTime, DateTimeZone}

object MoviesAnalytics {
  val spark = SparkSession
    .builder()
    .master("local")
    .appName(s"WojtekMalinowskiJob.at(${DateTime.now(DateTimeZone.UTC)})")
    .config("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
    .config("spark.kryoserializer.buffer", "2048")
    .getOrCreate()

  import spark.implicits._

  def getMoviesFromResources(): Dataset[Movie] = spark
    .read
    .text("./src/main/resources/movies.dat")
    .map(row => Movie(row.getString(0)))

  def getRatingsFromResources(): Dataset[MovieRating] = spark
    .read
    .text("./src/main/resources/ratings.dat")
    .map(row => MovieRating(row.getString(0)))

  def aggregateMovieRatings(ds: Dataset[MovieRating]) = ds
    .groupBy("movieId")
    .agg($"movieId", max("rating"), min("rating"), mean("rating"))

  def main(args: Array[String]): Unit = {
    val movies = getMoviesFromResources()
    val ratings = getRatingsFromResources()

    val movieRatings = movies.join(aggregateMovieRatings(ratings), "movieId")

    movies.write.parquet("./output_movies.parquet")
    ratings.write.parquet("./output_ratings.parquet")
    movieRatings.write.parquet("./output_movieRatings.parquet")
  }
}
