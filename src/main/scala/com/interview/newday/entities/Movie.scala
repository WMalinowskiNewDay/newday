package com.interview.newday.entities


case class Movie(movieId: Int, title: String, genres: Array[String])

object Movie {
  def apply(csvLine: String): Movie = {
    val fields = csvLine.split("::")
    Movie(fields(0).toInt, fields(1), fields(2).split('|'))
  }
}