package com.interview.newday.entities


case class User(userId: Int, gender: Char, ageGroup: Int, occupationCode: Int, zipCode: String)

object User {
  def apply(csvLine: String): User = {
    val fields = csvLine.split("::")
    User(fields(0).toInt, fields(1).charAt(0), fields(2).toInt, fields(3).toInt, fields(4))
  }
}
