package com.interview.newday.entities

case class MovieRating(userId: Int, movieId: Int, rating: Int, timestamp: Long)

object MovieRating {
  def apply(csvLine: String): MovieRating = {
    val fields = csvLine.split("::")
    MovieRating(fields(0).toInt, fields(1).toInt, fields(2).toInt, fields(3).toLong)
  }
}
